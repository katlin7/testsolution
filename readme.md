# Testassignment solution

## Classes:

- All files are in one package for the simplicity to reach them.
- Main.java -- main class
- results.txt is in Main class folder

**CLASSES:**

- **ConversionUtil** - tools for big decimal conversion
- **HouseResultService** - printing house final result to file
- **Main** - main class
- **Match** - Match POJO
- **MatchDa**o - parsing match data from "match_data.txt"
- **Player** - Player POJO
- **PlayerData** - PlayerData POJO
- **PlayerDataDao** - parsing player data from "player_data.txt"
- **PlayerFactory** - creates list of players
- **PlayerFinalResult** - player final result POJO
- **PlayerService** - methods to do calculations with players and matching the values of player/match
- **ResultService** - methods for printing output data to "results.txt"

**ENUMS:**

- **Side** - enum values of A, B, DRAW
- **Transaction** - enum values of transactions 


## Description

**The testassignment solution.**

I have implemented the checks for some of the methods (for the others I did not have the time anymore, so there are checks that are missing).

**Checks are done with:**

- player_data.txt -- if any of these values - player id, transaction type, coin number - is missing or wrong format, then this line in the file is skipped. (PlayerDataDao class)
- match_data.txt -- if any of the values of match is missing, the line is skipped. (MatchDao class)
- some of the methods in PlayerService are checked for the possible NullPointerExceptions etc.

## Prerequisites to run the code

- Maven
- (Project uses Lombok library for annotations)

## Launch procedure

- git clone https://gitlab.com/katlin7/testsolution.git
- the Main class is in Main.java

## Todo:
See description section.

## Author
Katlin Kalde 


