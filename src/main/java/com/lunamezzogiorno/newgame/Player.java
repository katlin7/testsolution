package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    private UUID id;
    private List<PlayerData> listOfTransactions;

    public Player(UUID id) {
        this.id = id;
    }

    public Long getPlayerBetCount(List<PlayerData> listOfTransactions) {
        return listOfTransactions
                .stream()
                .filter(Objects::nonNull)
                .filter(t -> Objects.nonNull(t.getSideOfTheMatch()))
                .filter(t -> t.getSideOfTheMatch().equals(Side.A)
                        || t.getSideOfTheMatch().equals(Side.B)
                        || t.getSideOfTheMatch().equals(Side.DRAW))
                .count();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
