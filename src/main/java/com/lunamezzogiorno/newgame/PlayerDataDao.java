package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDataDao {
    List<PlayerData> playerData;

    public PlayerData createPlayerDataObject(String[] values) {
        PlayerData playerData = new PlayerData();

        if (!values[0].isBlank()) {
            playerData.id(uuidFromString(values[0]));
        }
        if (!values[1].isBlank()) {
            playerData.transaction(Transaction.valueOf(values[1]));
        }
        if (!values[2].isBlank()) {
            playerData.match(new Match(uuidFromString(values[2])));
        }
        if (!values[3].isBlank()) {
            playerData.coinNumber(Integer.parseInt(values[3]));
        }
        if (!values[4].isBlank()) {
            playerData.sideOfTheMatch(Side.valueOf(values[4]));
        }
        return playerData;
    }

    private UUID uuidFromString(String str) {
        return UUID.fromString(str);
    }

    //create players data objects from file
    public List<PlayerData> getPlayersDataFromFile(File file) {
        playerData = new ArrayList<>();
        String fileLine;
        String[] values;

        try (var fr = new FileReader(file);
             var br = new BufferedReader(fr)) {
            while ((fileLine = br.readLine()) != null) {
                values = fileLine.split(",", -1);

                //Create player only if player has valid UUID and valid transaction:
                if (isUuidValid(values[0]) && isTransactionValid(values[1]) && isCoinNumberValid(values[3])) {
                    playerData.add(createPlayerDataObject(values));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return playerData;
    }

    public boolean isUuidValid(String uuid) {
        Pattern UUID_REGEX = Pattern.compile(
                "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        );
        return UUID_REGEX.matcher(uuid).matches();
    }

    public boolean isTransactionValid(String transaction) {
        return (transaction.equalsIgnoreCase(String.valueOf(Transaction.BET)) ||
                transaction.equalsIgnoreCase(String.valueOf(Transaction.DEPOSIT)) ||
                transaction.equalsIgnoreCase(String.valueOf(Transaction.WITHDRAW)));
    }

    public boolean isCoinNumberValid(String coinNumber) {
        try {
            Double.parseDouble(coinNumber);
            return true;
        } catch (Exception e) {
            System.out.println("Coin number missing or NaN, skipping the line!");
            return false;
        }
    }

    public TreeSet<UUID> getAllUniqueIds(List<PlayerData> playerData) {
        TreeSet<UUID> uuids = new TreeSet<>();
        playerData
                .forEach(pd -> uuids.add(pd.getId()));
        return uuids;
    }
}
