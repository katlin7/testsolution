package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerFinalResult implements Comparable<PlayerFinalResult> {
    //to create for any player their result object
    private String id;
    private Long balance;
    private BigDecimal winRate;
    private Boolean illegal;

    @Override
    public int compareTo(PlayerFinalResult results) {
        return id.compareToIgnoreCase(results.id);
    }
}
