package com.lunamezzogiorno.newgame;

public enum Transaction {
    DEPOSIT,
    BET,
    WITHDRAW
}
