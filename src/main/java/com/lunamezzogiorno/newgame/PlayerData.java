package com.lunamezzogiorno.newgame;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
public class PlayerData implements Comparable<PlayerData> {
    private UUID id;
    private Transaction transaction;
    private Match match;
    private Integer coinNumber;
    private Side sideOfTheMatch;

    public PlayerData() {
    }

    public PlayerData id(UUID UUID) {
        this.id = UUID;
        return this;
    }

    public PlayerData match(Match match) {
        this.match = match;
        return this;
    }

    public PlayerData transaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    public PlayerData coinNumber(int coinNumber) {
        this.coinNumber = coinNumber;
        return this;
    }

    public PlayerData sideOfTheMatch(Side sideOfTheMatch) {
        this.sideOfTheMatch = sideOfTheMatch;
        return this;
    }

    @Override
    public int compareTo(PlayerData pd) {
        return convertToString(this.id)
                .compareTo(convertToString(pd.id));
    }

    public String convertToString(UUID uuid) {
        return uuid.toString();
    }

}
