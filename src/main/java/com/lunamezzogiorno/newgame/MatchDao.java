package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MatchDao {
    private List<Match> matches;
    private File file;

    public void getMatchData() throws IOException {
        matches = new ArrayList<>();
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {
            String fileLine;
            while ((fileLine = br.readLine()) != null) {
                String[] values = fileLine.split(",", -1);
                if (isUuidValid(values[0]) && isResultOfTheMatchValid(values[3])) {
                    matches.add(new Match(
                            UUID.fromString(values[0]),
                            Double.parseDouble(values[1]),
                            Double.parseDouble(values[2]),
                            Side.valueOf(values[3])
                    ));
                }
            }
        }
    }

    public boolean isUuidValid(String uuid) {
        Pattern UUID_REGEX = Pattern.compile(
                "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        );
        return UUID_REGEX.matcher(uuid).matches();
    }

    public boolean isResultOfTheMatchValid(String resultString) {
        return (resultString.equalsIgnoreCase(String.valueOf(Side.A)) ||
                resultString.equalsIgnoreCase(String.valueOf(Side.B)) ||
                resultString.equalsIgnoreCase(String.valueOf(Side.DRAW)));
    }
}
