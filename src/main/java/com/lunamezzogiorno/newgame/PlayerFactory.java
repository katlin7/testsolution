package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerFactory {
    private List<Player> players;

    //Create players
    public void createPlayers(PlayerDataDao playerDataDao, File playerFile) {
        players = new ArrayList<>();

        List<PlayerData> playersData = playerDataDao.getPlayersDataFromFile(playerFile);
        TreeSet<UUID> playersActions = playerDataDao.getAllUniqueIds(playersData);

        for (UUID uuid : playersActions) {
            Player player = new Player();
            player.setId(uuid);
            player.setListOfTransactions(new ArrayList<>());
            playersData.stream()
                    .filter(pd -> pd.getId().equals(uuid))
                    .forEach(pd -> player.getListOfTransactions().add(pd));
            players.add(player);
        }
    }
}
