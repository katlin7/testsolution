package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.IOException;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlayerService {
    private Player player;
    private int transactionNumber;              //shows which row is it
    private Long balance;                       //balance: player current balance
    private boolean illegalAction;
    private MatchDao matchDao;
    private int winCounter;                     //counter reading matches won
    private Long houseBalance;                  //house balance here

    public List<Match> getMatchDao() throws IOException {
        matchDao.getMatchData();
        return matchDao.getMatches();
    }

    public List<PlayerData> getPlayerTransactions() {
        return player.getListOfTransactions();
    }

    public PlayerData getCurrentPlayerData() {
        return getPlayerTransactions().get(transactionNumber);
    }

    public Long getPlayerBets() {
        return player.getPlayerBetCount(getPlayerTransactions());
    }

    //get players current match data
    public Match getCurrentMatch() {
        try {
            return getCurrentPlayerData().getMatch();
        } catch (NullPointerException e) {
            System.out.println("Current match is null, skipping the value! " + e.getMessage());
            return null;
        }
    }

    public void setCurrentMatchData() throws IOException {
        var playerMatch = getCurrentMatch();
        if (playerMatch != null) {
            getMatchDao()
                    .stream()
                    .filter(m -> m.getId().equals(playerMatch.getId()))
                    .findFirst().ifPresent(m -> getCurrentPlayerData().setMatch(m));
        }
    }


    public Transaction getTransactionType() {
        return getCurrentPlayerData().getTransaction();
    }

    public int getCoinNumber() {
        return getCurrentPlayerData().getCoinNumber();
    }

    //Side on which player wagered
    public Side getSideOfTheMatch() {
        return getCurrentPlayerData().getSideOfTheMatch();
    }

    //Side who won
    public Side getResultOfTheMatch() {
        try {
            return getCurrentMatch().getResultOfTheMatch();
        } catch (NullPointerException e) {
            System.out.println("Current match is null, skipping the value! " + e.getMessage());
            return null;
        }
    }

    public Double getReturnRateA() {
        return getCurrentMatch().getReturnRateA();
    }

    public Double getReturnRateB() {
        return getCurrentMatch().getReturnRateB();
    }


    public void getIllegalActions() {
        int coinNumber = getCoinNumber();
        switch (getTransactionType()) {
            case BET, WITHDRAW -> {
                if (this.balance < coinNumber) {
                    illegalAction = true;
                }
            }
        }
    }

    public Double setReturnRate() {
        Double rate;
        if (getSideOfTheMatch() == null) {
            return null;
        }
        switch (getSideOfTheMatch()) {
            case A -> rate = getReturnRateA();
            case B -> rate = getReturnRateB();
            default -> rate = null;
        }
        return rate;
    }

    //set betResult (win: if 1, draw: 0, lose: -1) to use it in getBetResult():
    public Integer setBetStringAsInt() {
        //Returns 0 if there is no data -- the coins will not be taken / given to anyone
        if (getResultOfTheMatch() == null) {
            return 0;
        }
        if (getSideOfTheMatch() == null) {
            return 0;
        }

        var currentResult = getResultOfTheMatch();
        var playerSide = getSideOfTheMatch();

        if (currentResult.equals(Side.DRAW)) return 0;
        else if (currentResult.equals(playerSide)) return 1;
        else return -1;

    }

    public int countWins() {
        if (setBetStringAsInt() == 1) {
            return winCounter++;
        }
        return winCounter;
    }

    //how many coins the player has got or lost after bet (incl.those which player placed on bet):
    public Integer getBetResult() {
        if (getCurrentMatch() != null) {
            switch (setBetStringAsInt()) {
                case 1 -> {
                    return (int) (getCoinNumber() * setReturnRate());
                }
                case 0 -> {
                    return 0;
                }
                case -1 -> {
                    return -getCoinNumber();
                }
                default -> {
                    return null;
                }
            }
        }
        return null;
    }

    public Long setBalanceAfterTransaction() {
        switch (getTransactionType()) {
            case DEPOSIT -> balance += getCoinNumber();
            case BET -> balance += getBetResult();
            case WITHDRAW -> balance -= getCoinNumber();
        }
        return balance;
    }

    public Long setHouseBalanceAfterBet() {
        switch (getTransactionType()) {
            case BET -> houseBalance -= getBetResult();
        }
        return houseBalance;
    }
}
