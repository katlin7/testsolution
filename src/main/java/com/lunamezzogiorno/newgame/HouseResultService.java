package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Data
@AllArgsConstructor
public class HouseResultService {
    private Long finalResult;

    public void writeToFile(File file) {
        try (var fileWriter = new FileWriter(file, true);
             var bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write("\n" + finalResultToString());
        } catch (IOException e) {
            System.out.println(e.getMessage() + " in class: " + this.getClass());
        }
    }

    public String finalResultToString() {
        return finalResult.toString();
    }
}
