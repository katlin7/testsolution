package com.lunamezzogiorno.newgame;

import java.math.BigDecimal;

public class ConversionUtil {
    public static BigDecimal convertLongToBigDecimal(Long number) {
        return BigDecimal.valueOf(number);
    }

    public static BigDecimal convertIntegerToBigDecimal(Integer number) {
        return BigDecimal.valueOf(number);
    }
}
