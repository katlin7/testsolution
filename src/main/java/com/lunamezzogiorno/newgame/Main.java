package com.lunamezzogiorno.newgame;

import lombok.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.regex.Pattern;


public class Main {

    public static List<PlayerFinalResult> getLegalPlayersResults(PlayerService playerService, List<Player> players) throws IOException {
        List<PlayerFinalResult> legalResultList = getLegalResults();
        playerService.setHouseBalance(0L);
        for(Player p: players) {

            //Initialize each player and their data:
            playerService.setPlayer(p);
            playerService.setBalance(0L);
            playerService.setIllegalAction(false);
            playerService.setWinCounter(0);

            //get only the players who have played:
            if(playerService.getPlayerBets() > 0) {
                //get a player's results:
                getPlayerFinalBalance(playerService);

                var winCount = ConversionUtil.convertIntegerToBigDecimal(playerService.getWinCounter());
                var betCount = ConversionUtil.convertLongToBigDecimal(playerService.getPlayerBets());
                var winRate = winCount.divide(betCount, 2, RoundingMode.HALF_EVEN);
                var illegal = playerService.isIllegalAction();

                if (!illegal)
                    legalResultList.add(new PlayerFinalResult(p.getId().toString(), playerService.getBalance(), winRate, illegal));

            }

        }
        return legalResultList;
    }

    public static List<PlayerData> getIllegalPlayersResults(PlayerService playerService, List<Player> players) throws IOException {
        List<PlayerData> illegalResultList = getIllegalResults();
        playerService.setHouseBalance(0L);
        for(Player p: players) {

            //Initialize each player and their data:
            playerService.setPlayer(p);
            playerService.setBalance(0L);
            playerService.setIllegalAction(false);
            playerService.setWinCounter(0);

            //get a player's results:
            getPlayerFinalBalance(playerService);

            var illegal = playerService.isIllegalAction();
            if(illegal)
                illegalResultList.add(playerService.getCurrentPlayerData());
        }
        return illegalResultList;
    }

    public static Long getPlayerFinalBalance(PlayerService playerService) throws IOException {
        //Player transactions:
        for(int i = 0; i < playerService.getPlayerTransactions().size(); i++) {
            if(playerService.isIllegalAction()) {
                return playerService.getBalance();
            }
            playerService.setTransactionNumber(i);
            getAllPlayersResults(playerService);
        }
        return playerService.getBalance();
    }

    public static List<PlayerFinalResult> getLegalResults(){
        return new ArrayList<>();
    }

    public static List<PlayerData> getIllegalResults(){
        return new ArrayList<>();
    }

    public static Long houseFinaleBalance(Long balance){
        return balance;
    }


    public static void getAllPlayersResults(PlayerService playerService) throws IOException {
        playerService.getPlayer();
        playerService.getPlayerTransactions();
        playerService.getCurrentPlayerData();
        playerService.setCurrentMatchData();
        playerService.getIllegalActions();     //void
        if(playerService.isIllegalAction()) {
            System.out.println("ILLEGAL ACTION!");
            return;
        }
        playerService.countWins();
        playerService.setReturnRate();
        playerService.getCurrentPlayerData().getSideOfTheMatch();
        playerService.getBetResult();
        playerService.setBalanceAfterTransaction();
        playerService.setHouseBalanceAfterBet();
    }

    public static void main(String[] args) throws IOException {

        File playersFile = new File("target/classes/player_data.txt");
        File matchesFile = new File("target/classes/match_data.txt");
        MatchDao matchDao = new MatchDao();
        matchDao.setFile(matchesFile);

        PlayerDataDao pdd = new PlayerDataDao();

        // Get list of all players data objects
        // (each row of file player_data.txt is an object in list)
        List<PlayerData> playersData = pdd.getPlayersDataFromFile(playersFile);
        System.out.println("ID-s of players:");
        System.out.println(pdd.getAllUniqueIds(playersData));

        //Create players (void):
        PlayerFactory playerFactory = new PlayerFactory();
        playerFactory.createPlayers(pdd, playersFile);

        //Create PlayerService:
        PlayerService playerService = new PlayerService();
        playerService.setMatchDao(matchDao);
        playerService.setHouseBalance(0L);


        ResultService rs = new ResultService();

        rs.setLegalResults(getLegalPlayersResults(playerService, playerFactory.getPlayers()));
        rs.setIllegalResults(getIllegalPlayersResults(playerService, playerFactory.getPlayers()));

        //Write to file:
        File outputFile = new File("src/main/java/com/lunamezzogiorno/newgame/result.txt");
        rs.writeToFile(outputFile);

        //Write hours to file:
        HouseResultService hrs = new HouseResultService(playerService.getHouseBalance());
        hrs.writeToFile(outputFile);

        System.out.println("House final balance: " + houseFinaleBalance(playerService.getHouseBalance()));
    }
}