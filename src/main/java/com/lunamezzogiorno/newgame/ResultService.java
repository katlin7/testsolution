package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultService {
    private List<PlayerFinalResult> legalResults;
    private List<PlayerData> illegalResults;

    public void writeToFile(File file) {
        try (var fileWriter = new FileWriter(file);
             var bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(formatTextToPrint().toString());
        } catch (IOException e) {
            System.out.println(e.getMessage() + " in class: " + this.getClass());
        }
    }

    public StringBuilder formatTextToPrint() {
        sortLegalResults();
        sortIllegalResults();
        StringBuilder stringBuilder = new StringBuilder();

        for (PlayerFinalResult lr : legalResults) {
            stringBuilder.append(lr.getId());
            stringBuilder.append(" ");
            stringBuilder.append(lr.getBalance());
            stringBuilder.append(" ");
            stringBuilder.append(lr.getWinRate());
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n");

        for (PlayerData ir : illegalResults) {

            stringBuilder.append(ir.getId().toString());
            stringBuilder.append(" ");
            stringBuilder.append(ir.getTransaction());
            stringBuilder.append(" ");
            if (ir.getMatch() != null)
                stringBuilder.append(ir.getMatch().getId().toString());
            else stringBuilder.append("null");
            stringBuilder.append(" ");
            stringBuilder.append(ir.getCoinNumber());
            stringBuilder.append(" ");
            stringBuilder.append(ir.getSideOfTheMatch());
            stringBuilder.append("\n");
        }
        return stringBuilder;
    }

    public void sortLegalResults() {
        legalResults.sort(PlayerFinalResult::compareTo);
    }

    public void sortIllegalResults() {
        illegalResults.sort((PlayerData::compareTo));
    }
}
