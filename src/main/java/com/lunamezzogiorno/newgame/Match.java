package com.lunamezzogiorno.newgame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Match {
    private UUID id;
    private double returnRateA;
    private double returnRateB;

    private Side resultOfTheMatch;

    public Match(UUID id) {
        this.id = id;
    }

}
